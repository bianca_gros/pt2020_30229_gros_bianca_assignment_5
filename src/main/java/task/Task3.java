package task;

import data.MonitoredData;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Task3 {
    private final String fileName;

    public Task3() {
        fileName = "Task_3.txt";
    }

    public List<MonitoredData> doTask3(){
        Task1 t1 = new Task1();
        List<MonitoredData> m = t1.doTask1();
        Map<String, Long> occurences = m.stream()
                .collect(Collectors.groupingBy(n -> n.getActivityLabel(), Collectors.counting()));
        writeToFile(occurences);
        return m;
    }

    private void writeToFile(Map<String, Long> map){
        try(Writer writer = Files.newBufferedWriter(Paths.get(fileName))) {
            map.forEach((key, value) -> {
                try {
                    writer.write(key + ": " + value + " times" + System.lineSeparator());
                }
                catch (IOException ex) {
                    throw new UncheckedIOException(ex);
                }
            });
        } catch(UncheckedIOException | IOException ex) {
            ex.printStackTrace();
        }
    }
}
