package task;

import data.MonitoredData;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Task1 {
    private final String fileName;
    private List<MonitoredData> data;

    public Task1(){
        fileName = "Task_1.txt";
        data = new ArrayList<>();
    }

    public List<MonitoredData> getData(){
        return this.data;
    }

    private void writeToFile(List<MonitoredData> m){
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(fileName, "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        m.forEach(writer::println);
        writer.close();
    }

    public List<MonitoredData> doTask1(){
        List<String> lines = null;
        try (Stream<String> stream = Files.lines(Paths.get("Activities.txt"))){
            lines = stream.flatMap(string -> Stream.of(string.split("\n")))
                    .collect(Collectors.toList());
        }
        catch(IOException e){
            e.printStackTrace();
        }
        this.data = lines.stream()
                .map(s -> s.split("\t\t"))
                .map(monitored -> new MonitoredData(monitored[0], monitored[1], monitored[2]))
                .collect(Collectors.toList());
        writeToFile(data);
        return data;
    }
}
