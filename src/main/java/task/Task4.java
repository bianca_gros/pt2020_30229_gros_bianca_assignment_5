package task;

import data.MonitoredData;

import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Task4 {
    private final String fileName;

    public Task4(){
        this.fileName = "Task_4.txt";
    }

    public  Map<Integer, Map<String, Integer>> doTask4(){
        Task1 t1 = new Task1();
        List<MonitoredData> monitoredData = t1.doTask1();
        Map<Integer,Map<String,Integer>> howMuch = monitoredData.stream()
                .collect(Collectors.groupingBy(t -> t.getStartTime().getDate(),Collectors.groupingBy(t -> t.getActivityLabel(),Collectors.summingInt(t -> 1))));
        writeToFile(howMuch);
        return howMuch;
    }

    private void writeToFile(Map<Integer,Map<String,Integer>> howMuch){
        try(Writer writer = Files.newBufferedWriter(Paths.get(fileName))) {
            howMuch.entrySet().stream().forEach(f -> {
                try {
                    writer.write("Day "+ f.getKey() + " - activities and times: "+ f.getValue().toString());
                    writer.write("\n");
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            });
        } catch(IOException ex) {
            ex.printStackTrace();
        }
    }
}
