package task;

import data.MonitoredData;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;

public class Task2 {
    private final String fileName;

    public Task2() {
        fileName = "Task_2.txt";
    }

    public int doTask2(){
        int count = 0;
        Task1 t1 = new Task1();
        List<MonitoredData> m = t1.doTask1();
        count = (int)m.stream().
                map(l -> l.getStartTime().toString().substring(5, 10))
                .distinct()
                .count();
        writeToFile(count);
        return count;
    }

    private void writeToFile(int i){
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(fileName, "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        writer.println("Total number of days appearing in the monitored data is " + i + ".");
        writer.close();
    }
}
