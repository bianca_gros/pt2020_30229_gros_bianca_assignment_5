package task;

import data.MonitoredData;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.AbstractMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Task5 {
    private final String fileName;

    public Task5() {
        this.fileName = "Task_5.txt";
    }

    public Map<String, Integer> doTask5() {
        Task1 t1 = new Task1();
        List<MonitoredData> monitoredData = t1.doTask1();
        Map<String, Integer> duration = monitoredData.stream().
                map(x -> new AbstractMap.SimpleEntry<String, Integer>(x.getActivityLabel(), (int)((x.getEndTime().getTime() - x.getStartTime().getTime()) / 1000)))
                .collect(Collectors.groupingBy(f -> f.getKey(), Collectors.summingInt(value -> value.getValue())));
        writeToFile(duration);
        return duration;
    }

    private void writeToFile(Map<String, Integer> duration){
        try(Writer writer = Files.newBufferedWriter(Paths.get(fileName))) {
            duration.forEach((key, value) -> {
                try {
                    writer.write(key + " = " + value + " seconds (" + value/3600 + "h:" + (value/60)%60 + "min:" + value%60 + "s)" + System.lineSeparator());
                }
                catch (IOException ex) {
                    throw new UncheckedIOException(ex);
                }
            });
        } catch(UncheckedIOException | IOException ex) {
            ex.printStackTrace();
        }
    }
}