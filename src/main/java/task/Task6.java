package task;

import data.MonitoredData;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Task6 {
    private final String fileName;

    public Task6(){
        fileName = "Task_6.txt";
    }

    public List<String> doTask6(){
        Task1 t1 = new Task1();
        List<MonitoredData> monitoredData = t1.doTask1();
        Map<String, Long> all = monitoredData.stream()
                .collect(Collectors.groupingBy(a -> a.getActivityLabel(), Collectors.counting()));
        Map<String, Long> lessThan5 = monitoredData.stream()
                .filter(m -> (m.getEndTime().getTime() - m.getStartTime().getTime()) < 300000)
                .collect(Collectors.groupingBy(a -> a.getActivityLabel(), Collectors.counting()));
        List<String> freq = lessThan5.entrySet()
                .stream()
                .filter(element -> ((element.getValue() * 100 / all.get(element.getKey())) > 90) && (all.containsKey(element.getKey())))
                .map(element -> element.getKey())
                .distinct()
                .collect(Collectors.toList());
        writeToFile(freq);
        return freq;
    }

    private void writeToFile(List<String> s){
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(fileName, "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        writer.write("The activities that have more than 90% occurences under 5 minutes are: ");
        s.forEach(writer::println);
        writer.close();
    }
}
