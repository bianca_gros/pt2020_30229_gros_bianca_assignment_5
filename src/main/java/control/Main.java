package control;

import task.*;

public class Main {

    public static void main(String[] args){
        Task1 t1 = new Task1();
        t1.doTask1();

        Task2 t2 = new Task2();
        t2.doTask2();

        Task3 t3 = new Task3();
        t3.doTask3();

        Task4 t4 = new Task4();
        t4.doTask4();

        Task5 t5 = new Task5();
        t5.doTask5();

        Task6 t6 = new Task6();
        t6.doTask6();
    }
}
