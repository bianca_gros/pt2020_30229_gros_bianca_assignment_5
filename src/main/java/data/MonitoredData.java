package data;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MonitoredData {
    private Date startTime;
    private Date endTime;
    private String activityLabel;

    public MonitoredData(){

    }

    public MonitoredData(String dateStart, String dateEnd, String activity){
        this.startTime = null;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            startTime = format.parse(dateStart);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        this.endTime = null;
        try {
            endTime = format.parse(dateEnd);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        this.activityLabel = activity;
    }

    public MonitoredData(Date start, Date end, String act){
        this.startTime = start;
        this.endTime = end;
        this.activityLabel = act;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getActivityLabel() {
        return activityLabel;
    }

    public void setActivityLabel(String activityLabel) {
        this.activityLabel = activityLabel;
    }

    @Override
    public String toString() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateStart = format.format(startTime);
        String dateEnd = format.format(endTime);
        return dateStart +
                "\t\t" + dateEnd +
                "\t\t" + activityLabel;
    }
}
